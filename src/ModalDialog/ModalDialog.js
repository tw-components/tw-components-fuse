import React, { Component } from "react";
import { Dialog, DialogContent, DialogTitle } from "@material-ui/core";

interface Props {
  open: Boolean;
  onClose: Function;
  title: String;
  content: Object;
}

export default class ModalDialog extends Component<Props> {
  render() {
    return (
      <div>
        <Dialog
          open={this.props.open}
          onClose={this.props.onClose}
          aria-labelledby="new-dialog-title"
          fullWidth={true}
        >
          <DialogTitle id="new-dialog-title">{this.props.title}</DialogTitle>
          <DialogContent className="bg-white" dividers={true}>
            {this.props.content}
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}
