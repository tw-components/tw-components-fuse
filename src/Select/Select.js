import React from "react";
import { Select as MaterialSelect, Typography } from "@material-ui/core";
import PropTypes from "prop-types";

export default class Select extends React.Component {
  render() {
    return (
      <div
        style={{
          flexDirection: "column",
          marginLeft: 4,
          marginRight: 4,
          width: "auto"
        }}
      >
        <Typography style={{ fontSize: 12 }}>{this.props.label}</Typography>
        <MaterialSelect
          className="bg-white flex h-40 rounded-4 pl-8"
          disableUnderline
          variant="outlined"
          value={this.props.value}
          onChange={this.props.onChange}
        >
          {this.props.items}
        </MaterialSelect>
      </div>
    );
  }
}

Select.propTypes = {
  items: PropTypes.object,
  label: PropTypes.string,
  value: PropTypes.object,
  onChange: PropTypes.func
};
