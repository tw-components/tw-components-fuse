# tw-components-fuse

> Componentes básicos para projetos que utilizam o Fuse como template

## Install

```bash
yarn add https://gitlab.com/tw-components/tw-components-fuse.git
```

## Usage ModalDialog

```jsx
import React, { Component } from "react";
import { ModalLoading } from "tw-components-fuse";

class Example extends Component {
  render() {
    return (
      <ModalDialog
        open={this.state.open}
        onClose={() =>
          this.setState({
            open: false
          })
        }
        title={"Título da Modal"}
        content={
          <>
            <div>Corpo da modal</div>
          </>
        }
      />
    );
  }
}
```

## Usage Select

```jsx
import React, { Component } from "react";
import { Select } from "tw-components-fuse";

class Example extends Component {
  render() {
    return (
      <Select
        label="Status"
        value={request.Filter.Status}
        onCha
        nge={ev => updateFilterStatus(ev)}
        name="filter"
        disableUnderline
        items={StatusEsteiraEnum.map(item => (
          <MenuItem key={item.id} value={item.id}>
            {item.value}
          </MenuItem>
        ))}
      />
    );
  }
}
```

![Utilização](https://i.imgur.com/wiDx9Gu.png)

## License

MIT © [Target Work](https://gitlab.com/tw-components/tw-components-fuse)
